package main

import (
	"fmt"
)

func main() {
	s := "blaubeere"
	isuniquehashmap(s)
	isuniqueAscii(s)

}

// Approach 1: Hash Map

func create_occurence_map(s string) (m map[string]int) {
	m = make(map[string]int)
	for _, c := range s {
		value, check_key_existance := m[string(c)]
		if check_key_existance == false {
			m[string(c)] = 1
		} else {
			m[string(c)] = value + 1
		}

	}
	return m
}
func printMap(m map[string]int) {
	for key, value := range m {

		fmt.Printf("Key: %s, Value: %d \n", key, value)
	}
}
func isuniquehashmap(s string) {
	m := create_occurence_map(s)
	for _, value := range m {
		if value > 1 {
			fmt.Println(s + " is not unique")
			return
		}
	}
	fmt.Println(s + " is unique")
}

// ASCII CODE
func isuniqueAscii(s string) {
	// assumption only 128 unique char in ascii
	if len(s) > 128 {
		fmt.Println(s + " is not unique")
	}
	booleanlist := make([]byte, 128)
	fmt.Println(booleanlist)
	for _, char := range s {
		if booleanlist[char] != 0 {
			fmt.Println(s + " is not unique")
			return
		} else {
			booleanlist[char] = 1
		}
	}
	fmt.Println(s + " is unique")
}
