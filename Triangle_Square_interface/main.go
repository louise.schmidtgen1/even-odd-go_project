package main

import "fmt"

type shape interface {
	getArea() float64
}

type square struct {
	sideLength float64
}
type triangle struct {
	height float64
	base   float64
}

func main() {
	mytriangle := triangle{
		height: 1,
		base:   2,
	}
	mysquare := square{
		sideLength: 2,
	}
	printArea(mytriangle)
	printArea(mysquare)
}

func (s square) getArea() (area float64) {
	return s.sideLength * s.sideLength
}

func (t triangle) getArea() (area float64) {
	return 0.5 * t.base * t.height
}

func printArea(s shape) {
	fmt.Println("Area: ", s.getArea())
}
