package main

import "fmt"

type contactInfo struct {
	email   string
	zipCode int
}
type person struct {
	firstName string
	lastName  string
	contactInfo
}

func main() {
	louise := person{
		firstName: "Louise",
		lastName:  "Schmidtgen",
		contactInfo: contactInfo{
			email:   "louise.schmidtgen@gmail.com",
			zipCode: 94000,
		},
	}
	// &variable Give me the memory address of the value this variable is pointing at
	// *pointer Give me the value this memory address is pointing at
	// only worry about pointers with int, float, string, bool, structs
	// not with slices maps, channels, pointers, functions
	louise.updateName("Lulu")
	louise.print()
}

func (p person) print() {
	fmt.Printf("%+v", p)
}

func (pointerToPerson *person) updateName(newFirstName string) {
	// This only updates the copy that is in this function
	// p.firstName = newFirstName
	(*pointerToPerson).firstName = newFirstName
}
