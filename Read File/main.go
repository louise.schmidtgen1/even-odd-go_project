package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	// Get passed in filename from environment using OS package
	filename := os.Args[1]

	fmt.Println(filename)
	//Open file using Open function in os package
	file, err := os.Open("myfile.txt") // For read access.
	if err != nil {                    // check if open file error >> does file exist in directory
		log.Fatal(err)
	}
	data := make([]byte, 100)     // make a byteslice
	count, err := file.Read(data) // Read the data from file put it in data byteslice
	if err != nil {               //check if error occured reading the file
		log.Fatal(err)
	}
	fmt.Printf("read %d bytes: %q\n", count, data[:count]) // print content of file
}
